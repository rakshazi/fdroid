#!/usr/bin/env python3
repo_url = "https://fdroid.rakshazi.me/repo"
repo_name = "Rakshazi F-Droid"
repo_icon = "rakshazi.png"
repo_description = """
FOSS apps, not included in main F-Droid repo.
Source: https://gitlab.com/rakshazi/fdroid
"""
archive_older = 10
archive_url = "https://does.not.exists"
archive_name = "Rakshazi F-Droid"
archive_icon = "rakshazi.png"
archive_description = "Does not exits"
repo_keyalias = "9294ac461a63"
keystore = "keystore.jks"
keydname = "CN=9294ac461a63, OU=F-Droid"
local_copy_dir = '/fdroid'
